@extends('layouts.default')
@section('content')
    <div class="flex-center position-ref full-height">
        <img src="https://b.zmtcdn.com/web_assets/81f3ff974d82520780078ba1cfbd453a1583259680.png" class="high-res-image" alt="" role="presentation" style="width:100%">
        <div class="searcharea">
            <div class="form-group has-feedback">
                <input type="text" class="form-control"  name="cityselection" id="cityselection" value="Chennai" readonly placeholder="Chennai"><span style="border-left:1px solid grey;"></span>
                <span class="glyphicon glyphicon-map-marker"></span>
                <input type="text" class="form-control"  name="frsearch" id="frsearch" placeholder="Search for resturant in this location"  >
                <span class="glyphicon glyphicon-search form-control-feedback"></span>
            </div>
            <div>
                <div class="listcontent" style="display:none"></div>
                <div class="searchcontent" style="display:none;position: absolute;right: 0;"></div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#frsearch').on('keyup',function(e) {
            var citysearch=$(this).val();
            var count=$(this).val().length;
            if(count>2){
                $.ajax({
                    type: "GET",
                    url: 'https://developers.zomato.com/api/v2.1/search?q='+citysearch+'&lat=13.0838890000000000&lon=80.2700000000000000',
                    headers: {
                        'Content-Type':'application/json',
                        'user-key':'5ffb698e3d9a8ea8d51fb8847c216058',
                    
                    },
                    success: function(data)
                    {
                        $('.searchcontent').css("display","block");
                        var content="";
                        var defaultimage='{{asset("assets/default.webp")}}';
                        console.log(data.restaurants);
                        var items = data.restaurants.map( (item,index)=> {
                            if(item.restaurant.thumb !=""){
                                content +='<a href="/restaurants?id='+item.restaurant.id+'"><div class="restaurentlist"><img src="'+item.restaurant.thumb+'" width=50 height=50><p>'+item.restaurant.name+'</p></div></a><br>';
                            }else{
                                content +='<a href="/restaurants?id='+item.restaurant.id+'"><div class="restaurentlist"><img src="'+defaultimage+'" width=50 height=50><p>'+item.restaurant.name+'</p></div></a><br>';
                            }
                        });
                        $('.searchcontent').html(content);
                    }
                });
            }
        });
        $(document).on('click','.citylist',function(){
            $('#cityselection').val($(this).text());
            $('.citylist').text('');
        });
        $(document).on('click','.restaurentlist',function(){
            $('#frsearch').val($(this).text());
            $('.restaurentlist').text('');
        });
    });
    </script>
@endsection
