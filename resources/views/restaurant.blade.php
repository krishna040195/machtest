@extends('layouts.default')
@section('content')
        <div class=" position-ref full-height">
            <div class="foodimage">
                <img src="" style="width:100%;height:60vh;"/>
            </div>   
<div id="exTab1" class="container ">
    <h1 class="hotelname" style="font-weight:600;color:#000"></h1>	
    <h4 class="cusines" style="color:#888"></h4>
            <div class="tabset detailsheader">
                <input type="radio" name="tabset" id="tab1" class="1a" aria-controls="marzen" checked>
                <label for="tab1">Basic Restaurant Details</label>
                <input type="radio" name="tabset" id="tab2" class="2a" aria-controls="rauchbier">
                <label for="tab2">Reviews</label>
                <input type="radio" name="tabset" id="tab3" class="3a" aria-controls="dunkles">
                <label for="tab3">Menu</label>
                <div class="tab-panels detailscontent">
                  <section id="marzen" class="tab-panel details">
                    <div>
                        <h4>Address</h4>
                        <p class="hotelname"></p>
                        <p class="address"></p>
                        </div>
                        <div>
                        <h4 >Cuisines</h4>
                        <p class="cusines"></p>
                        </div>
                        <div>
                        <h4 >Average Cost</h4>
                        <p class="avgcost"></p>
                        </div><div>
                        <h4>Timings</h4><p class="timings"></p></div><div>
                        <h4>Contact</h4><p class="phone_numbers"></p></div>
                </section>
                  <section id="rauchbier" class="tab-panel">
                    <div class="listcontentprofile"></div>
                  </section>
                  <section id="dunkles" class="tab-panel">
                    <div class="menuitems" >
                    </div>
                  </section>
                </div>
              </div>
  </div>
  @endsection
  @section('scripts')
    <script>
        let searchParams = new URLSearchParams(window.location.search)
        searchParams.has('id') // true
        let param = searchParams.get('id');
        

        if(param != ""){
                    $.ajax({
                        type: "GET",
                        url: 'https://developers.zomato.com/api/v2.1/restaurant?res_id='+param,
                        headers: {
        'Content-Type':'application/json',
        'user-key':'5ffb698e3d9a8ea8d51fb8847c216058',
       
    },
                        success: function(data)
                        {
                            console.log(data);
                            var hotelname=data.name;
                            $('.hotelname').text(hotelname);
                            $('.foodimage img').attr('src',data.featured_image);
                            $('.cusines').text(data.cuisines);
                            $('.avgcost').text(data.average_cost_for_two);
                            $('.address').text(data.location.address);
                            $('.timings').text(data.timings);
                            $('.phone_numbers').text(data.phone_numbers);
                            


                       }
                   });
                    }
                   $(document).ready(function(){
                    $('.2a').on('click',function(){
                        $.ajax({
                        type: "GET",
                        url: 'https://developers.zomato.com/api/v2.1/reviews?res_id='+param,
                        headers: {
        'Content-Type':'application/json',
        'user-key':'5ffb698e3d9a8ea8d51fb8847c216058',
       
    },
                        success: function(data)
                        {
                           
                            $('.listcontentprofile').css("display","block");
                            var content="";
                            var items = data.user_reviews.map( (item,index)=> {
                                 content +='<div style="display: flex;-webkit-box-flex: 1;flex-grow: 1; overflow: hidden; -webkit-box-pack: center;justify-content: center;-webkit-box-align: center;align-items: center;"><div ><img src="'+item.review.user.profile_image+'" style="height:4.4rem;width:4.4rem;border-radius: 50%;"/></div><div style="margin-left: 1.2rem;-webkit-box-flex: 1;flex-grow: 1;    overflow: hidden;"><p style="font-size: inherit;line-height: 1.5;    color: rgb(54, 54, 54);    size: 1.6rem;    overflow: hidden;    text-overflow: ellipsis;    -webkit-box-flex: 1;    flex-grow: 1;    margin: 0px 0px 0.2rem;    font-weight: 500;}">'+item.review.user.name+'</p><p>Ratings : '+item.review.rating+'</p></div></div><p style="border-bottom: 1px solid #cbbfbf;padding-bottom: 15px; margin-bottom: 15px;">'+item.review.review_text+'</p>';
                                
                            });
                            
                            $('.listcontentprofile').html(content);

                       }
                   });
                    });
                    $('.3a').on('click',function(){
                        $.ajax({
                        type: "GET",
                        url: 'https://developers.zomato.com/api/v2.1/dailymenu?res_id='+param,
                        headers: {
        'Content-Type':'application/json',
        'user-key':'5ffb698e3d9a8ea8d51fb8847c216058',
       
    }, statusCode: {
        400: function(responseObject, textStatus, jqXHR) {
            var content = "<div><p>No Menu item is found for this restaurent</p></div>"
            $('.menuitems').html(content);
        },
        503: function(responseObject, textStatus, errorThrown) {
            // Service Unavailable (503)
            // This code will be executed if the server returns a 503 response
        }  ,         
                        success: function(data)
                        {
                           
    }
                            // $('.menuitems').css("display","block");
                            // var content="";
                            // // var items = data.user_reviews.map( (item,index)=> {
                            // //      content +='<div style="display: flex;-webkit-box-flex: 1;flex-grow: 1; overflow: hidden; -webkit-box-pack: center;justify-content: center;-webkit-box-align: center;align-items: center;"><div ><img src="'+item.review.user.profile_image+'" style="height:4.4rem;width:4.4rem;border-radius: 50%;"/></div><div style="margin-left: 1.2rem;-webkit-box-flex: 1;flex-grow: 1;    overflow: hidden;"><p style="font-size: inherit;line-height: 1.5;    color: rgb(54, 54, 54);    size: 1.6rem;    overflow: hidden;    text-overflow: ellipsis;    -webkit-box-flex: 1;    flex-grow: 1;    margin: 0px 0px 0.2rem;    font-weight: 500;}">'+item.review.user.name+'</p><p>Ratings : '+item.review.rating+'</p></div></div><p style="border-bottom: 1px solid #cbbfbf;padding-bottom: 15px; margin-bottom: 15px;">'+item.review.review_text+'</p>';
                                
                            // // });


                       }
                   });
                    });
                   });

    </script>
@endsection

